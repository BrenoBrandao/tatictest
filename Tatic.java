/*
Breno Brandão Gonçalves
*/
import java.io.*;
import java.util.Scanner;

public class Tatic{  

  static long[] dates = new long[9999999];
  static String[] hexaId = new String[9999999]; 
  static long[] userId = new long[9999999];

   /* MAIN */
  public static void main(String[] args){
    Scanner sc = new Scanner(System.in);  
    try{
      
      BufferedReader br = new BufferedReader(new FileReader("sample.txt"));
      String linha = "";
      linha = br.readLine();
      System.out.println("Armazenando dados...");
      for (int i = 0; i<=2067854; i++){
        
        dates[i] = Long.parseLong(linha.substring(0,17));
        hexaId[i] = linha.substring(18,26);
        userId[i] = Long.parseLong(linha.substring(27,linha.length()));
        //System.out.println(dates[i]+";"+hexaId[i]+";"+userId[i] + " " + i);
        linha = br.readLine();
      }
      int cmd = 0;
      do{
        System.out.println("Digite 1 para realizar uma pesquisa ou 0 para sair:");
        cmd = sc.nextInt();
        if(cmd != 1 && cmd != 0) 
          System.out.print("Valor inválido. ");
      }while(cmd != 1 && cmd != 0);
      
      long dateB = 0, dateF = 0;
      String[] ids = new String[999999];
      int idsCount = 0;

      while(cmd != 0){

        System.out.println("Digite a data inicial:");
        dateB = sc.nextLong();
        System.out.println("Digite a data final:");
        dateF = sc.nextLong();
        int cmd2 = 1;
        System.out.println("Digite um identificador de evento ou 0 para iniciar a pesquisa:");
        String id = sc.next();
        while(id.equals("0") == false){
          ids[idsCount] = id;
          System.out.println("Digite outro identificador de evento ou 0 para iniciar a pesquisa:");
          id = sc.next();
          idsCount++;
        }
        
        buscador(dateB,dateF,ids,idsCount);
        System.out.println("Digite 1 para realizar outra pesquisa ou 0 para sair:");
        cmd = sc.nextInt();
      }
        


    }catch(Exception erro){
      System.out.println("Falha ao ler o arquivo!");
    }

  }//FIM DO MAIN

  public static void buscador(long dateB, long dateF, String[] ids, int idsCount){
    int start = 0, end = 0;

    while(dates[start] != dateB){
      start++;
    }

    for (end = start; dates[end] != dateF; end++) 
    end++;

    if (idsCount > 0) {
      for (int i = 0; i < idsCount; i++) {
        for (int k = start; k<=end; k++) {
          if (hexaId[k].equals(ids[i])) {
            System.out.println(dates[k]+";"+hexaId[k]+";"+userId[k]);
          }
        }
      }
    }
    else{
      for (; start<=end; start++) {
        System.out.println(dates[start]+";"+hexaId[start]+";"+userId[start]);
      }
    }
  }
      

}//FIM DA CLASSE Tatic
